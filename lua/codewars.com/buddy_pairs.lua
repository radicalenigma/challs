local solution = {}

local cache = {}

function sum(n) 
	if cache[n] then return cache[n] end
	local val = 1
	local squared = math.floor(math.sqrt(n))
	for i = 2,squared,1 do
		if n % i == 0 then val = val + i + n / i end
	end
	if squared * squared == n then val = val - squared end
	cache[n] = val
	return val
end

function solution.buddy(start, limit)
	for i = start,limit,1 do
		local candidate = sum(i) - 1
		if sum(candidate) - 1 == i and candidate > i then return {i, candidate} end
	end
	return {-1, -1}
end

return solution

--print(unpack(solution.buddy(10, 50)))
--print(unpack(solution.buddy(48, 50)))
--print(unpack(solution.buddy(810, 2755)))
--print(sum(48))
--print(sum(75))
