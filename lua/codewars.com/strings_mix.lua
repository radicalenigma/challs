local solution = {}

function freq(s)
    local f = {}
    for c in s:gmatch("([a-z])") do
        if f[c] == nil then f[c] = {c, 0} end
        f[c][2] = f[c][2] + 1
    end
    return filter(f)
end

function filter(tbl) 
    local rval = {}
    for k, v in pairs(tbl) do
        if v[2] > 1 then rval[k] = v end
    end
    return rval
end

function solution.mix(s1, s2)
    local f1 = freq(s1)
    local f2 = freq(s2)

    local tmp = {}
    for c in string.gmatch("abcdefghijklmnopqrstuvwxyz", ".") do
        local c1 = f1[c]
        local c2 = f2[c]
        if c1 ~= nil and c2 ~= nil then
            local cc1 = c1[2]
            local cc2 = c2[2]
            if cc1 > cc2 then
                tmp[#tmp + 1] = {cc1, "1:" .. c:rep(cc1), c}
            elseif cc2 > cc1 then
                tmp[#tmp + 1] = {cc2, "2:" .. c:rep(cc2), c}
            else
                tmp[#tmp + 1] = {cc1, "=:" .. c:rep(cc1), c}
            end
        elseif c1 ~= nil then
            tmp[#tmp + 1] = {c1[2], "1:" .. c:rep(c1[2]), c}
        elseif c2 ~= nil then
            tmp[#tmp + 1] = {c2[2], "2:" .. c:rep(c2[2]), c}
        end
    end

    table.sort(
        tmp,
        function(a,b)
            if a[1] ~= b[1] then return a[1] > b[1] end
            return a[2] < b[2]
        end)

    local output = {}
    for _, v in pairs(tmp) do output[#output + 1] = v[2] 
    end
    return table.concat(output, "/")
end

print(solution.mix('codewars', 'codewars'))

--local sol = freq("A aaaa bb c")
--for k, v in pairs(sol) do
--    print(k, v)
--end


--return solution
