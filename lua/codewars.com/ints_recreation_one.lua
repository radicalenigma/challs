local solution = {}

local cache = {}

function divs(n) 
    if n == 1 then return 1 end
	if cache[n] then return cache[n] end
    local val = 1 + n*n
    local i = 2
    while i <= math.floor(math.sqrt(n)) do
        if n % i == 0 then
            val = val + i * i
            if i ~= n / i then 
                val = val + (n/i) * (n/i)
            end
        end
        i = i + 1
    end
    cache[n] = val
	return val
end

function solution.listSquared(m, n)
    local squares = {}
    local bozos = {}
    for i = m,n do 
        local s = divs(i)
        if bozos[s] == nil then
            local sq = math.floor(math.sqrt(s))
            bozos[s] = sq * sq == s
        end
        if bozos[s] then squares[#squares + 1] = {i, s} end
    end
	return squares
end


--print(divs(246))
--print(divs(4))
return solution
