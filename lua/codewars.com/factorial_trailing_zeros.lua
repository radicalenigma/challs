local solution = {}

function solution.zeros(n)
	local zeros = 0
	for i = 1,15,1 do
		zeros  = zeros + math.floor(n / math.pow(5, i))
	end
	return zeros
end

return solution

--print(solution.zeros(6))
--print(solution.zeros(30))
--print(solution.zeros(152))
